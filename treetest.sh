#!/bin/bash
# Create some repos, the last one is our combined repo.  'a' is the remote for the subtree, 'root' is remote for root tree, whole is a 
# standard remote for the whole thing
declare -a repo_list=('root_remote' 'subtree_remote' 'combined_remote' 'combined')
# kill old graph views:
kill $(ps -ef | grep 'gitk' | grep -v 'grep' | awk {'print $2'})

#Git uses 1s time stamps.  $sleep keeps commits in order but slows down the script

sleep=echo
cwd=`pwd`

count=0
for repo in ${repo_list[*]};
do
  cd ${cwd}
  rm -rf "testrepos/${repo}"   # harded coded safety
  mkdir -p "testrepos/${repo}"
  cd "testrepos/${repo}"
  git init
  git checkout -b null
  git commit -m "initial null commit" --allow-empty
done

git checkout --orphan master

# The labels chosen here will randomly impact order of the subtrees processed.
# Admitedly these were chosen to keep a history that looks better in gitk, 
# but only because gitk forces master to the left and merges from the right.
# If you really care about order use the subtree specific push/pull commands.
${cwd}/git-alltrees add root-tree / ../root_remote
${cwd}/git-alltrees add subtree subtree/ ../subtree_remote

echo $((count++)) >  c1-both    # add a file to root tree
mkdir subtree
mkdir ours 
echo $((count++)) >  subtree/c1-both   # and to subtree
echo $((count++)) >  ours/c1-both      # and annull to root 
git config pull.rebase false
git add --all
git commit -a -m "commit 1, local both"
$sleep 1  # allow git to understand the commit order

echo $((count++)) >  subtree/c2-subonly              # just subtree
#echo $((count++)) >  subtree/c2.b-subonly              # just subtree
git add --all
git commit -a -m "commit 2, local subtree"
$sleep 1

echo $((count++)) >  ours/c3-rootonly                  #just root
git add --all
git commit -a -m "commit 3, local root"
$sleep 1

echo $((count++)) >  subtree/c4-both 
echo $((count++)) >  ours/c4-both         #both
git add --all
git commit -a -m "commit 4, local both"
$sleep 1

echo "******************************************* Pushing Trees**********************************"
${cwd}/git-alltrees push --all master   # push all trees
echo "******************************************* Pulling Trees**********************************"
${cwd}/git-alltrees pull --all master   # pull all trees
echo "******************************************* Pushing to Origin**********************************"
#Push to the combined repo
git remote add origin `readlink -f ../combined_remote`
git push origin master 


# add file to subtree remote
cd ../subtree_remote
git checkout master
echo $((count++)) >  c5-subtree-remote
git add --all
git commit -a -m "commit 5, subtree remote"
git checkout null
$sleep 1

# add file to root remote
cd ../root_remote
git checkout master
echo $((count++)) >  c6-root-remote
git add --all
git commit -a -m "commit 6, root remote"
git checkout null
$sleep 1


# add file to combined remote
cd ../combined_remote
git checkout master
echo $((count++)) >  c7-combined-remote
git add --all
git commit -a -m "commit 7, combined remote"
git checkout null
$sleep 1

#back home..
cd ../combined
git checkout master
echo $((count++)) >  c8-both
echo $((count++)) >  subtree/c8-both
#rm subtree/c2.b-subonly
git add --all
git commit -a -m "commit 8, local both"
$sleep 1

echo $((count++)) >  subtree/c9-subtree
git add --all
git commit -a -m "commit 9, local subtree"


git status
echo `pwd`
echo '******************************************* Pulling From Origin****************************'
#git pull origin master --allow-unrelated-histories --no-edit
git pull origin master --no-edit
echo '******************************************* Pulling Trees**********************************'
#${cwd}/git-alltrees pull / master   # pull all trees
${cwd}/git-alltrees pull --all master   # pull all trees
echo '******************************************* Pushing Trees**********************************'
${cwd}/git-alltrees push --all master   # push all trees
echo '******************************************* Pushing to Origin******************************'
git push origin master   # push to combined remote

Now we have pretty different histories, can they still sync up?



cd ../root_remote
git checkout master
echo $((count++)) >  c10-root-remote
git add --all
git commit -a -m "commit 10, root remote"
git checkout null
$sleep 1
cd ../combined

echo '******************************************* Pulling Trees**********************************'
${cwd}/git-alltrees pull --all master   # pull all trees
echo '******************************************* Pushing Trees**********************************'
${cwd}/git-alltrees push --all master   # push all trees
echo '******************************************* Pushing to Origin******************************'
git push origin master   # push to combined remote


cd ../combined
git checkout master


# launch views
for repo in ${repo_list[*]};
do
  cd ${cwd}
  cd "testrepos/${repo}"
  git checkout master   # so we can show file tree
  gitk master&
done

cd ${cwd} 
tree testrepos
cd testrepos/combined

for repo in ${repo_list[*]};
do
  cd ${cwd}
  cd "testrepos/${repo}"
  git checkout null   # so we can show file tree
done

cd ../combined
git checkout master



