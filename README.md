## git-alltrees  <br><br>  ...(see [The Wiki](https://gitlab.com/douglas.s.leonard/alltrees/-/wikis/home)  for details.)  

Copyright Douglas Leonard, 2021, all rights reserved. MIT Expat license, basically, use it as you wish, just don't misrepresent it as your work.  Credit for the method is of course great.  Absolutely no warranty of any kind.


----

**Subtrees without duplicates or empty merges even without squash, preserved tree repo branching history, no initial splitting step, and a new parent-tree, root-tree or container-tree support.**




